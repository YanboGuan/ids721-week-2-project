use aws_sdk_s3::Client as S3Client;
use lambda_http::{service_fn, Body, Error as LambdaError, Request, Response};
use serde::{Deserialize, Serialize};
use aws_config::meta::region::RegionProviderChain;
use tracing_subscriber::{self, EnvFilter, fmt::Subscriber};

#[derive(Debug, Deserialize)]
struct CsvRecord {
    list: String,
}

#[derive(Serialize)]
struct ApiResponse {
    original_lists: Vec<String>, // To store original lists
    total_sum: i32,
}

async fn function_handler(_event: Request) -> Result<Response<Body>, LambdaError> {
    let region_provider = RegionProviderChain::default_provider().or_else("us-east-1");
    let config = aws_config::load_from_env().await;

    let client = S3Client::new(&config);
    let bucket = "mini2yb";
    let key = "mini2data.csv";

    let resp = client.get_object()
                     .bucket(bucket)
                     .key(key)
                     .send()
                     .await
                     .map_err(|e| LambdaError::from(e.to_string()))?;
                     
    let body_bytes = resp.body.collect().await.map_err(|e| LambdaError::from(e.to_string()))?.into_bytes();
    let csv_content = std::str::from_utf8(&body_bytes).map_err(|e| LambdaError::from(e.to_string()))?;

    let mut rdr = csv::Reader::from_reader(csv_content.as_bytes());
    let mut total_sum = 0;
    let mut original_lists = Vec::new();

    for result in rdr.deserialize::<CsvRecord>() {
        if let Ok(record) = result {
            original_lists.push(record.list.clone()); // Add the original list to the collection
            let list_sum: i32 = record.list.trim_matches(|c| c == '[' || c == ']')
                                           .split(',')
                                           .filter_map(|s| s.parse::<i32>().ok())
                                           .sum();
            total_sum += list_sum;
        }
    }

    let api_response = ApiResponse {
        original_lists,
        total_sum,
    };

    let resp = Response::builder()
        .status(200)
        .header("content-type", "application/json")
        .body(serde_json::to_string(&api_response).unwrap().into())
        .map_err(Box::new)?;

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    let subscriber = Subscriber::builder()
        .with_env_filter(EnvFilter::new("INFO"))
        .finish();

    tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    lambda_http::run(service_fn(function_handler)).await?;
    Ok(())
}
