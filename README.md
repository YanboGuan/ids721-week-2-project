# yg229_mini_project_2

This AWS Lambda function that interacts with Amazon S3 to perform sum operation of a list on a CSV file. 

## Author

Yanbo Guan

## API Gateway
[Link](https://wqg87aw2pe.execute-api.us-east-1.amazonaws.com/default/new-lambda-project)

## Configuration and Initialization:

It initializes the AWS SDK for Rust with a region provider, defaulting to "us-east-1" if no region is specified in the environment.
Sets up a logging subscriber using tracing_subscriber to filter logs at the INFO level.

## S3 Interaction:
The function creates an S3 client using the AWS SDK configuration loaded from the environment.
It then retrieves an object from an S3 bucket named "mini2yb" with a key "mini2data.csv", which is expected to be a CSV file.

## CSV Processing:

The content of the CSV file is read as a string and processed using the csv crate to deserialize it into records based on the CsvRecord struct, which expects each record to contain a list field as a string.

## Response Construction:

After processing all records, the function constructs an ApiResponse struct containing the vector of original list strings and the total sum of all numbers found in the CSV file.
This response struct is then serialized into a JSON string and included in the body of an HTTP response with a 200 OK status and application/json content type.

## Input CSV:
```
list
"[1,3,5,7,9,11,13]"

```

## Run on local:
```cargo lambda watch```

![Local result](/local.png "Demo on local")

## Delpoy on AWS:
1. ```cargo lambda build --release```
2. ```cargo lambda deploy```


## API Gateway Integration result
![API Gateway Integration](/addapi.png "Add API Gateway")

## Run on AWS:
![AWS Result](/api.png "Dem on AWS")